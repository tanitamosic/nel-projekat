import random
import copy
from ann_criterion import optimality_criterion as f
import json
import time

global_best = []
global_fbest = float('inf')


def add_to_json(opt, results):
    global global_best, global_fbest
    new_data = {
        "particles": opt.particles,
        "maxiter": opt.maxiter,
        "initial_pba": opt.initial_pba,
        "final_pba": opt.final_pba,
        "initial_gba": opt.initial_gba,
        "final_gba": opt.final_gba,
        "initial_inertia": opt.initial_inertia,
        "final_inertia": opt.final_inertia,
        "results": results
    }
    with open("results.json", "r") as f:
        data = json.load(f)
        data.append(new_data)
    with open("results.json", "w") as f:
        json.dump(data, f, indent=4)
    return


class Particle:


    # noinspection PyUnusedLocal
    def __init__(self, dimension, func):
        self.position = [(random.random() - 0.5) * 2 for i in range(dimension)]
        self.fx = float('inf')
        self.personal_best = []
        self.best_fx = float('inf')
        self.velocity = [(random.random() - 0.5) * 2 for i in range(dimension)]
        self.dimension = dimension
        self.adjust(func)

    def _calculate_components(self, xmax, xmin, tmax, t):
        x = xmin + (xmax - xmin) / tmax * (tmax - t)  # changes component value by each iteration
        return x

    def update(self, opt, it):
        global global_fbest, global_best
        inertia = self._calculate_components(opt.final_inertia, opt.initial_inertia, opt.maxiter, it)
        cp = self._calculate_components(opt.final_pba, opt.initial_pba, opt.maxiter, it)
        cg = self._calculate_components(opt.final_gba, opt.initial_gba, opt.maxiter, it)

        for i in range(self.dimension):
            rp = random.random()
            rg = random.random()

            self.velocity[i] = inertia * self.velocity[i] + cp * rp * (self.personal_best[i] - self.position[i]) + \
                               cg * rg * (global_best[i] - self.position[i])
            self.position[i] = self.position[i] + self.velocity[i]

    def adjust(self, func):
        global global_fbest, global_best
        self.fx = func(self.position)
        if self.fx < global_fbest:
            self._update_globals(self.fx)
            self._update_personals(self.fx)
        elif self.fx < self.best_fx:
            self._update_personals(self.fx)

    def _update_globals(self, fx):
        global global_fbest, global_best
        global_fbest = fx
        global_best = copy.deepcopy(self.position)

    def _update_personals(self, fx):
        self.personal_best = copy.deepcopy(self.position)
        self.best_fx = fx


class Options:
    def __init__(self):
        self.particles = 30            # The number of particles.
        self.maxiter = 100             # The number of maxiter.
        self.initial_pba = 2.4175      # Initial value of the individual-best acceleration factor.
        self.final_pba = 0.4705        # Final value of the individual-best acceleration factor.
        self.initial_gba = 0.5         # Initial value of the global-best acceleration factor.
        self.final_gba = 2.63          # Final value of the global-best acceleration factor.
        self.initial_inertia = 0.837   # Initial value of the inertia factor.
        self.final_inertia = 0.218     # Final value of the inertia factor.


def PSO(func, options, dimension=60):
    # noinspection PyUnusedLocal
    populace = [Particle(dimension, func) for i in range(options.particles)]

    for i in range(options.maxiter):
        for p in populace:
            p.update(options, i)
            p.adjust(func)

    # print(populace[0].global_best)
    print(global_fbest)
    populace.clear()
    return global_fbest


def reset_globals():
    global global_fbest, global_best
    global_best = []
    global_fbest = float('inf')
    return


if __name__ == '__main__':
    # noinspection PyListCreation
    t = time.time()
    results = []
    opt = Options()
    for k in range(3):
        results.append(PSO(f, opt))
        reset_globals()
    add_to_json(opt, results)
    print(time.time() - t)
